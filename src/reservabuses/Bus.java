/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservabuses;

/**
 *
 * @author juaco
 */
public class Bus {

    private String placa;
    private String color;
    private Persona sillas[][];
    private String empresa;
    private Conductor conductor;
    private Persona asistente = new Persona("1054921438", "Juan Carlos", "Emir Becerra", "30");

    public Bus(String placa, String color, Persona[][] sillas, String empresa, Conductor conductor) {
        
        this.placa = placa;
        this.color = color;
        this.empresa = empresa;
        this.conductor = conductor;

        this.sillas = new Persona[20][4];
        this.sillas[0][0] = conductor;
        this.sillas[0][1] = conductor;
        this.sillas[0][2] = asistente;
        this.sillas[0][2] = asistente;
        this.sillas[19][0] = asistente;
        this.sillas[19][3] = asistente;

    }

    public Bus() {
    }
    

    public String getPlaca() {
        return placa;

    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
    
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the sillas
     */
    public Persona[][] getSillas() {
        return sillas;
    }

    /**
     * @param sillas the sillas to set
     */
    public void setSillas(Persona[][] sillas) {
        this.sillas = sillas;
    }

    /**
     * @return the empresa
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the conductor
     */
    public Conductor getConductor() {
        return conductor;
    }

    /**
     * @param conductor the conductor to set
     */
    public void setConductor(Conductor conductor) {
        this.conductor = conductor;
    }

    /**
     * @return the asistente
     */
    public Persona getAsistente() {
        return asistente;
    }

    /**
     * @param asistente the asistente to set
     */
    public void setAsistente(Persona asistente) {
        this.asistente = asistente;
    }
    
    
}
