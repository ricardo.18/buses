/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservabuses;

/**
 *
 * @author juaco
 */
public class Pasajero extends Persona{
    
    private String codigoPasajero;
    
    public Pasajero(String codigoPasajero, String numeroDocumento, String nombre, String apellido, String edad) {
        super(numeroDocumento, nombre, apellido, edad);
        this.codigoPasajero = codigoPasajero;
        
        
    }
    
    public Pasajero(){
        
    }


    /**
     * @return the codigoPasajero
     */
    public String getCodigoPasajero() {
        return codigoPasajero;
    }

    /**
     * @param codigoPasajero the codigoPasajero to set
     */
    public void setCodigoPasajero(String codigoPasajero) {
        this.codigoPasajero = codigoPasajero;
    }
    
    @Override
    public String toString(){
        return super.toString()
                +"\nCodigo Pasajero: "+codigoPasajero;
    }
    
}
