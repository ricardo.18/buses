/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservabuses;

/**
 *
 * @author juaco
 */
public class Reserva {
    private String salida;
    private String destino;
    private EmpresaTuristica empresa;
    private Pasajero pasajero;
    private Bus bus;
    private Conductor conductor;

    public Reserva(String salida, String destino, EmpresaTuristica empresa, Pasajero pasajero, Bus bus,
                    Conductor conductor) {
        this.salida = salida;
        this.destino = destino;
        this.empresa = empresa;
        this.pasajero = pasajero;
        this.bus = bus;
        this.conductor = conductor;
    }

    /**
     * @return the partida
     */
    public String getPartida() {
        return salida;
    }

    /**
     * @param partida the partida to set
     */
    public void setPartida(String partida) {
        this.salida = partida;
    }

    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    /**
     * @return the empresa
     */
    public EmpresaTuristica getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(EmpresaTuristica empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the pasajero
     */
    public Pasajero getPasajero() {
        return pasajero;
    }

    /**
     * @param pasajero the hora to set
     */
    public void setPasajero(Pasajero pasajero) {
        this.pasajero = pasajero;
    }

    /**
     * @return the bus
     */
    public Bus getBus() {
        return bus;
    }

    /**
     * @param bus the bus to set
     */
    public void setBus(Bus bus) {
        this.bus = bus;
    }

    /**
     * @return the conductor
     */
    public Conductor getConductor() {
        return conductor;
    }

    /**
     * @param conductor the conductor to set
     */
    public void setConductor(Conductor conductor) {
        this.conductor = conductor;
    }
    
    
    
}
