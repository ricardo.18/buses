/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservabuses;

/**
 *
 * @author juaco
 */
public class Conductor extends Persona{
    private String tipoLicencia;
    private String codigoEmpresa;

    
    public Conductor(String tipoLicencia, String codigoEmpresa, String numeroDocumento, String nombre, String apellido, String edad) {
        super(numeroDocumento, nombre, apellido, edad);
        this.tipoLicencia = tipoLicencia;
        this.codigoEmpresa = codigoEmpresa;
    }

    public Conductor() {
    }
     
    

    /**
     * @return the tipoLicencia
     */
    public String getTipoLicencia() {
        return tipoLicencia;
    }

    /**
     * @param tipoLicencia the tipoLicencia to set
     */
    public void setTipoLicencia(String tipoLicencia) {
        this.tipoLicencia = tipoLicencia;
    }

    /**
     * @return the codigoEmpresa
     */
    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    /**
     * @param codigoEmpresa the codigoEmpresa to set
     */
    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }
    
    @Override
    
    public String toString(){
        return super.toString()
                +"\nTipo de licencia: "+tipoLicencia
                +"\nCodigo Empresarial: "+codigoEmpresa;
    }
}
