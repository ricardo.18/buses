/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservabuses;

/**
 *
 * @author juaco
 */
public class Asistente extends Persona{
    private String direccion;

    public Asistente(String direccion, String numeroDocumento, String nombre, String apellido, String edad) {
        super(numeroDocumento, nombre, apellido, edad);
        this.direccion = direccion;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
